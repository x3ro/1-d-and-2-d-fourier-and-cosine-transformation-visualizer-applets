# 1-D and 2-D Fourier / Cosine Transformation Visualizer

This is a guide to get and run the Java applets of the University of Mannheim, especially the applets for the 1-D and 2-D Fourier and cosine transformation visualizers.



## Links

- [Applets (University of Mannheim)][applets]
- [AdoptJDK (OpenJDK 10)][adoptopenjdk-10]



## How to start the applets

*The applets need the `appletviewer` binary which is included in OpenJDK up to
version 10.*



### Get the Correct JAVA Version


If your installed version of Java does not include the `appletviewer`
anymore, download a JRE/JDK that comes with the `appletviewer` from
[AdoptJDK][adoptopenjdk] (e.g. [OpenJDK 10][adoptopenjdk-10] which is the
last version to in clude the binary). You can choose to download a portable
version (`.zip` / `.tar.gz`) so you don't have to install an old version.

The binary file must be in the `bin` directory of the extracted JRE/JDK.



### Get the Applets

Download and extract the applets from the [University of Mannheim][applets].

Find the "*1-dimensional DCT and DFT*" and "*2-dimensional DCT*" and use the
"*Appletfiles (view applet offline)*" link to download the applet files.




### Start the Applet

Start the `appletviewer` with the path to the `index.html` of the applet as
argument:




#### On Windows

Either drag-and-drop the `index.html` onto the `appletviewer.exe`

Or open a terminal and run

```cmd
<path-to-jdk>\bin\appletviewer.exe <path-to-applet>\index.html
```

Put the above into a `launch-applet.cmd` to directly start the applet by
double-clicking the `.cmd` file.



#### On Linux / Mac

Open a terminal and run

```bash
<path-to-jdk>/bin/appletviewer <path-to-applet>/index.html
```

Put the following into a `launch-applet.sh` file to directly start the applet
by double-clicking the `.sh` file:

```bash
#!/usr/bin/env bash

<path-to-jdk>/bin/appletviewer <path-to-applet>/index.html
```





[applets]: https://pi4.informatik.uni-mannheim.de/pi4.data/content/animations/index.html
[adoptopenjdk]: https://adoptopenjdk.net/
[adoptopenjdk-10]: https://adoptopenjdk.net/releases.html?variant=openjdk10&jvmVariant=hotspot
